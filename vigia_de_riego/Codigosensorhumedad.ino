//Mrhobbyelectronics
//Website: http://www.mrhobbytronics.com
//YouTube: https://www.youtube.com/MrHobbytronics

#define BLYNK_PRINT Serial
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>

char auth[] = "6cfbc5f27a654be6823b663a624ce03b"; //colocar app token
char nombreWifi[]="";
char contrasenaWifi[]=""; //si no hay contrasena no colocar

void setup(){
  Serial.begin(115200);
  Blynk.begin(auth, nombreWifi, contrasenaWifi);
}

int humedad = 0;
boolean regado = true;
void loop(){ 
  if (!regado){
    humedad = map(analogRead(0), 0, 1024, 100, 0);
    Serial.print("humedad: ");
 Serial.println(humedad);
    if(humedad > 25){
       regado = true;
       Blynk.notify("Gracias Bo!");
    }
  }else{
    humedad = map(analogRead(0), 0, 1024, 100, 0);
    Serial.print("humedad: ");
    Serial.println(humedad);
    if (humedad <= 25) {  // 0-25 is red "dry"
       Serial.println ("necesito agua, muero de sed");
       Blynk.notify("Por favor regame!");
       regado = false;
       delay(1000);
    }  
  }
    Blynk.virtualWrite(V5, humedad);
    delay(200);
    Blynk.run();
  }

