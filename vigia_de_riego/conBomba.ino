#define BLYNK_PRINT Serial
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#define pinRele D0

char auth[] = "<Auth generado por la app>";
char nombreWifi[]="";
char contrasenaWifi[]="";

void setup(){
  Serial.begin(115200);
  Blynk.begin(auth, nombreWifi, contrasenaWifi);
}

int humedad = 0;
boolean regado = true;
void loop(){ 
  if (!regado){
    humedad = map(analogRead(0), 0, 1024, 100, 0);
    Serial.print("humedad: ");
 Serial.println(humedad);
    if(humedad > 25){
       regado = true;
       digitalWrite(pinRele, LOW);
       Blynk.notify("Gracias Bo!");
    }
  }else{
    humedad = map(analogRead(0), 0, 1024, 100, 0);
    Serial.print("humedad: ");
    Serial.println(humedad);
    if (humedad <= 25) {  // 0-25 is red "dry"
       Serial.println ("necesito agua, muero de sed");
       Blynk.notify("Por favor regame!");
       digitalWrite(pinRele, HIGH);
       regado = false;
       delay(1000);
    }  
  }
    Blynk.virtualWrite(V5, humedad);
    delay(200);
    Blynk.run();
  }

